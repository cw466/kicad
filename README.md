# Git Fundamentals
Celine Wang BME290

v2.0: Modified circuit to have toggle switch off the board and modified 1x6 pin to 1x8 pin

v2.0.1: Updated PCB to reflect removing toggle switch from board and changed 1x6 pin to 1x8 connecting pin
v2.0.2 updated the readme to record all the changes